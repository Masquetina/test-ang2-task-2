import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { User } from "./user.interface";

declare let firebase: any;

@Injectable()
export class AuthService {
  constructor(private router: Router) {}

  loginUser(user: User) {
    firebase.auth().signInWithEmailAndPassword(user.email, user.password)
      .then((res) => {
        if(res)
          console.log('Success!');
          this.router.navigate(['/protected']);
        })
      .catch(this.handleError);
  }

  private handleError (error: any) {
    console.log(error.code);
  }

  logout() {
    firebase.auth().signOut()
      .then(() => {
        this.router.navigate(['/login']);
      });
    }

    isAuthenticated() {
      let user = firebase.auth().currentUser;
      return !!user;
    }
}
