import { NgModule } from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent }   from './app.component';
import { HeaderComponent } from "./shared/header.component";
import { HomeComponent } from "./static/home.component";
import { LoginComponent } from "./login/login.component";
import { ProtectedComponent } from "./protected/protected.component";
import { FooterComponent } from "./shared/footer.component";
import { AuthGuard } from "./shared/auth.guard";
import { NonAuthGuard } from "./shared/non-auth.guard";
import { AuthService } from "./shared/auth.service";
import { routing } from "./app.routing";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    ProtectedComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    ReactiveFormsModule
  ],
  providers: [
    AuthGuard,
    NonAuthGuard,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
